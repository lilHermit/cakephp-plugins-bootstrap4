[![Packagist](https://img.shields.io/packagist/v/lilhermit/cakephp-plugin-bootstrap4.svg?style=flat-square)](https://packagist.org/packages/lilhermit/cakephp-plugin-bootstrap4) [![Bootstrap version](https://img.shields.io/badge/Bootstrap%20version-4.0.0-brightgreen.svg?style=flat-square)](https://getbootstrap.com/)  [![Twitter Follow](https://img.shields.io/twitter/follow/lilh3rmit.svg?style=social&label=Follow)](https://twitter.com/lilH3rmit)

# Bootstrap4 plugin

## We've moved

You can now find us at https://github.com/lilHermit/cakephp-plugins-bootstrap4